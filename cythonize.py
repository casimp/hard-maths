from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("hard_maths",  ["hard_maths.py"]),
    #   ... all your modules that need be compiled ...
]
setup(
    name = 'dataprocessing',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)